#!/usr/bin/env python

### sudo pip install git+https://github.com/sergeii/python-whois.git

import sys
import subprocess
from whois import whois
from pprint import pprint

### Check command-line arguments.
if ((len(sys.argv) < 2) or (len(sys.argv) > 3)):
	print("*** Usage: python deltawhois.py file (file)\n")
else:
	### bash things
	c = "sort -t\" \" -k2,2nr " + sys.argv[1] + " | cut -d\" \" -f1 > ipFile.txt"
	subprocess.call(c, shell=True)
	
	checkSet = []
	
	### Open the list of IP addresses and white list if provided
	ipFile = open("ipFile.txt", 'r')
	if (len(sys.argv) == 3):
		wlFile = open(sys.argv[2], 'r')
	
	### Read the files.
	ipList = ipFile.read().splitlines()
	if (len(sys.argv) == 3):
		wlList = wlFile.read().splitlines()
	
		### Make the white list set.
		wlSet = set(wlList)
	
		### Remove white listed IPs.
		checkSet = [x for x in ipList if x not in wlSet]
	else:
		checkSet = [x for x in ipList]
	
	### whois the complement of the IP list
	for i in checkSet:
		print(i)
		m = whois.whois(i)
		if 'orgname' in m:
			print("OrgName:\t" + str(m['orgname']))
		if 'country' in m:
			print("Country:\t" + str(m['country']).upper())
		if 'ipv4_from' and 'ipv4_to' in m:
			print("Netblock:\t" + str(m['ipv4_from']) + " - " + str(m['ipv4_to']))
		print("\n")
		
	### Close the file.
	ipFile.close()
	if (len(sys.argv) == 3):
		wlFile.close()	
