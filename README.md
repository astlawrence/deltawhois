# deltawhois

Python script used to parse IIS logs to determine source origin information.
Results could be used to generate ACLs.

### Requirements

* pip 
* pip install git+https://github.com/sergeii/python-whois.git

### Installation

git clone https://github.com/astlawrence/deltawhois.git


### Usage
```
python deltawhois.py IP_List (White_List)
```

